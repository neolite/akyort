# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150628125731) do

  create_table "articles", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.string   "slug",       limit: 255
    t.string   "seo_desc",   limit: 255
    t.string   "seo_kwords", limit: 255
    t.string   "image_id",   limit: 255
    t.text     "content",    limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "category",   limit: 255
    t.string   "status",     limit: 255
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",           limit: 255, null: false
    t.integer  "sluggable_id",   limit: 4,   null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope",          limit: 255
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "galleries", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.boolean  "published",  limit: 1
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "title",      limit: 255
  end

  create_table "gallery_items", force: :cascade do |t|
    t.integer  "gallery_id", limit: 4
    t.string   "title",      limit: 255
    t.string   "image_id",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "houses", force: :cascade do |t|
    t.string   "title",           limit: 255
    t.string   "slug",            limit: 255
    t.string   "keywords",        limit: 255
    t.string   "desc",            limit: 255
    t.float    "price",           limit: 24
    t.float    "price_for_meter", limit: 24
    t.string   "house_type",      limit: 255
    t.string   "material",        limit: 255
    t.integer  "etaj",            limit: 4
    t.text     "content",         limit: 65535
    t.float    "home_size",       limit: 24
    t.float    "land_area",       limit: 24
    t.string   "city_name",       limit: 255
    t.integer  "location_id",     limit: 4
    t.boolean  "published",       limit: 1
    t.boolean  "sold",            limit: 1
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "images", force: :cascade do |t|
    t.string   "file_id",    limit: 255
    t.integer  "house_id",   limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "images", ["house_id"], name: "index_images_on_house_id", using: :btree

  create_table "locations", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.string   "slug",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "pages", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.string   "seo_desc",   limit: 255
    t.string   "seo_kwords", limit: 255
    t.text     "content",    limit: 65535
    t.integer  "menu_pos",   limit: 4,     default: 0, null: false
    t.boolean  "published",  limit: 1
    t.boolean  "in_menu",    limit: 1
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "status",     limit: 255
    t.string   "image_id",   limit: 255
    t.string   "slug",       limit: 255
  end

  add_foreign_key "images", "houses"
end
