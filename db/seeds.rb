# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

%w(Миловка Акбердино Зубово Нагаево).each do |loc|
  Location.create title: loc, slug: Russian.transliterate(loc)
end

5.times do

end


