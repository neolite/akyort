class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.string :seo_desc
      t.string :seod_kwords
      t.text :content
      t.integer :menu_pos
      t.boolean :published
      t.boolean :in_menu

      t.timestamps null: false
    end
  end
end
