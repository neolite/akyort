class CreateBackendGalleryItems < ActiveRecord::Migration
  def change
    create_table :gallery_items do |t|
      t.integer :gallery_id
      t.string :title
      t.string :image_id

      t.timestamps null: false
    end
  end
end
