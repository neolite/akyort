class SetDefaultValuesForPages < ActiveRecord::Migration
  def change
    change_column :pages, :menu_pos, :integer, :null => false, :default => 0
  end
end
