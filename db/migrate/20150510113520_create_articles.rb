class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.string :slug
      t.string :seo_desc
      t.string :seo_kwords
      t.string :image
      t.text :content
      t.integer :category_id

      t.timestamps null: false
    end
  end
end
