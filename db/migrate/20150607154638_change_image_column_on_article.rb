class ChangeImageColumnOnArticle < ActiveRecord::Migration
  def change
      rename_column :articles, :image, :image_id
  end
end
