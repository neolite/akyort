class CreateBackendGalleries < ActiveRecord::Migration
  def change
    create_table :galleries do |t|
      t.string :name
      t.boolean :published

      t.timestamps null: false
    end
  end
end
