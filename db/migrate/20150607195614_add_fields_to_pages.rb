class AddFieldsToPages < ActiveRecord::Migration
  def change
    add_column :pages, :status, :string
    add_column :pages, :image_id, :string
  end
end
