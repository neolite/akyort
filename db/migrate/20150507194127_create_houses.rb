class CreateHouses < ActiveRecord::Migration
  def change
    create_table :houses do |t|
      t.string :title
      t.string :slug
      t.string :keywords
      t.string :desc
      t.float :price
      t.float :price_for_meter
      t.string :house_type
      t.string :material
      t.integer :etaj
      t.text :content
      t.float :home_size
      t.float :land_area
      t.string :city_name
      t.integer :location_id
      t.boolean :published
      t.boolean :sold

      t.timestamps null: false
    end
  end
end
