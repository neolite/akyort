class AddCategoryColumnToArticlesTable < ActiveRecord::Migration
  def change
    remove_column :articles, :category_id
    add_column :articles, :category, :string
  end
end
