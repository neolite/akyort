# Сайт недвижимости #

* Переводил свой проект на php на ruby
* Учился RoR на этом проекте

# Список гемов

* gem 'russian' - **чтобы все было по русски**
* gem 'twitter-bootstrap-rails', '> 3.0.0' - **бутстрап**
* gem 'simple_form', '> 3.0.0' - **простые формы быстро создаем**
* gem 'nested_form' - **для вложеных форм**
* gem 'cocoon' - **динамические вложенные формы**
* gem 'bootstrap_form' - **формы с разметкой для бутстрапа**

* gem 'annotate', '~> 2.6.6' - **чтобы в моделях были аннотации**
* gem 'responders', '~> 2.0'
* gem 'mini_magick' - **нужен для refile**
* gem 'refile', require: ["refile/rails", "refile/image_processing"] - **обработка файлов **

* gem 'friendly_id', '~> 5.1.0' - **сео ссылки**
* gem 'babosa'
* gem 'redactor-rails' - **клевый редактор**
* gem 'will_paginate', '~> 3.0.6'
* gem 'flexslider' - **прикольный слайдер**
* gem 'active_link_to'

* gem 'capistrano', '~> 3.4.0' - **хотел замутить деплой))**