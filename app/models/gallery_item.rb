# == Schema Information
#
# Table name: gallery_items
#
#  id         :integer          not null, primary key
#  gallery_id :integer
#  title      :string(255)
#  image_id   :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class GalleryItem < ActiveRecord::Base
  belongs_to :gallery
  attachment :image

end
