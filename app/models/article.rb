# == Schema Information
#
# Table name: articles
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  slug       :string(255)
#  seo_desc   :string(255)
#  seo_kwords :string(255)
#  image_id   :string(255)
#  content    :text(65535)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  category   :string(255)
#  status     :string(255)
#

class Article < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, :use => :slugged
  attachment :image
  enum category: {
           article: "Статья",
           post: "Пост блога"
  }
  enum status: {
           draft: "Черновик",
           published: "Опубликован",
           archived: "В архиве"
  }

  validates_presence_of :title, :content, :category
  validates_uniqueness_of :title

  validates :title, length: { minimum: 5 }
  validates :content, length: { minimum: 5 }

end
