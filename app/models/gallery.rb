# == Schema Information
#
# Table name: galleries
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  published  :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  title      :string(255)
#

class Gallery < ActiveRecord::Base
  has_many :gallery_items
  accepts_nested_attributes_for :gallery_items, :allow_destroy => true

  validates_presence_of :title, :name
  validates_uniqueness_of :title, :name

  validates :title, length: { minimum: 3 }
  validates :name, length: { minimum: 3 }
end
