# == Schema Information
#
# Table name: images
#
#  id         :integer          not null, primary key
#  file_id    :string(255)
#  house_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_images_on_house_id  (house_id)
#

class Image < ActiveRecord::Base
  belongs_to :house
  attachment :file
end
