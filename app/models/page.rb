# == Schema Information
#
# Table name: pages
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  seo_desc   :string(255)
#  seo_kwords :string(255)
#  content    :text(65535)
#  menu_pos   :integer          default(0), not null
#  published  :boolean
#  in_menu    :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  status     :string(255)
#  image_id   :string(255)
#  slug       :string(255)
#

class Page < ActiveRecord::Base
  extend FriendlyId

  friendly_id :title, :use => :slugged
  attachment :image

  enum status: {
           draft: "Черновик",
           published: "Опубликован",
           archived: "В архиве"
       }

  validates_presence_of :title, :content
  validates_uniqueness_of :title

  validates :title, length: { minimum: 5 }
  validates :content, length: { minimum: 5 }

  validates :menu_pos, :numericality => { :greater_than_or_equal_to => 0 }

end
