# == Schema Information
#
# Table name: houses
#
#  id              :integer          not null, primary key
#  title           :string(255)
#  slug            :string(255)
#  keywords        :string(255)
#  desc            :string(255)
#  price           :float(24)
#  price_for_meter :float(24)
#  house_type      :string(255)
#  material        :string(255)
#  etaj            :integer
#  content         :text(65535)
#  home_size       :float(24)
#  land_area       :float(24)
#  city_name       :string(255)
#  location_id     :integer
#  published       :boolean
#  sold            :boolean
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class House < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, :use => :slugged
  belongs_to :location
  has_many :images
  accepts_nested_attributes_for :images, :allow_destroy => true

  enum house_type: {
           house: "Загородный дом",
           cottage: "Коттедж",
           townhouse: "Таунхаус"
  }

  validates_presence_of :title, :price, :house_type, :etaj, :material, :home_size, :land_area, :content
  validates_uniqueness_of :title
  validates_numericality_of :price, :etaj, :home_size, :land_area

  end
