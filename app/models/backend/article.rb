# == Schema Information
#
# Table name: articles
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  slug       :string(255)
#  seo_desc   :string(255)
#  seo_kwords :string(255)
#  image_id   :string(255)
#  content    :text(65535)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  category   :string(255)
#  status     :string(255)
#

class Backend::Article < Article
  self.table_name = 'articles'
end
