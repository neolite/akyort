# == Schema Information
#
# Table name: galleries
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  published  :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  title      :string(255)
#

class Backend::Gallery < Gallery
  self.table_name = 'galleries'
end
