# == Schema Information
#
# Table name: pages
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  seo_desc   :string(255)
#  seo_kwords :string(255)
#  content    :text(65535)
#  menu_pos   :integer          default(0), not null
#  published  :boolean
#  in_menu    :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  status     :string(255)
#  image_id   :string(255)
#  slug       :string(255)
#

class Backend::Page < Page
  self.table_name = 'pages'
end
