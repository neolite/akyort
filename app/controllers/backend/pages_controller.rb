class Backend::PagesController < BackendController
  before_action :set_page, only: [:show, :edit, :update, :destroy]

  # GET /pages
  def index
    @pages = Backend::Page.all
  end

  # GET /pages/1
  def show
  end

  # GET /pages/new
  def new
    @page = Backend::Page.new
  end

  # GET /pages/1/edit
  def edit
  end

  # POST /pages
  def create
    @page = Backend::Page.new(page_params)
    # byebug
    if @page.save
      redirect_to @page, notice: 'Page was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /pages/1
  def update
    if @page.update(page_params)
      redirect_to @page, notice: 'Page was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /pages/1
  def destroy
    @page.destroy
    redirect_to backend_pages_url, notice: 'Страница была успешно удалена.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_page
      @page = Backend::Page.friendly.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def page_params
      params.require(:backend_page).permit(:title, :seo_desc, :seo_kwords, :content, :menu_pos, :in_menu, :status, :image)
    end
end
