class Backend::HousesController < BackendController
  before_action :set_house, only: [:show, :edit, :update, :destroy]

  # GET /houses
  # GET /houses.json
  def index
    @houses = Backend::House.order("created_at DESC").paginate(:page => params[:page])
  end

  # GET /houses/1
  # GET /houses/1.json
  def show
  end

  # GET /houses/new
  def new
    @house = Backend::House.new
  end

  # GET /houses/1/edit
  def edit
  end

  # POST /houses
  # POST /houses.json
  def create
    # @house = House.new(house_params.except(:images))
    @house = Backend::House.new(house_params)
    respond_to do |format|
      if @house.save
        @house.price_for_meter = @house.price / @house.home_size
        unless house_params[:images].blank?
          house_params[:images].each do |i|
            image = Image.new
            image.file = i
            image.save
            @house.images << image
          end
          @house.save
        end
        format.html { redirect_to backend_houses_path(@house), notice: 'Дом был успешно добавлен.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /houses/1
  # PATCH/PUT /houses/1.json
  def update
    # raise @house
    respond_to do |format|
      if @house.update(house_params)

        format.html { redirect_to backend_houses_url, notice: 'Дом был успешно обновлен.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /houses/1
  # DELETE /houses/1.json
  def destroy
    @house.destroy
    respond_to do |format|
      format.html { redirect_to backend_houses_url, notice: 'Дом был успешно удален.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_house
      @house = Backend::House.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def house_params
      params.require(:backend_house).permit( :title, :keywords, :desc, :price, :house_type, :material, :etaj, :content, :home_size, :land_area, :city_name, :location_id, :published, :sold, images: [], images_attributes: [:id, :file, :_destroy])
    end
end
