class Backend::GalleriesController < BackendController
  before_action :set_backend_gallery, only: [:show, :edit, :update, :destroy]

  # GET /backend/galleries
  def index
    @galleries = Backend::Gallery.order("created_at DESC").all
    # byebug
  end

  # GET /backend/galleries/1
  def show
  end

  # GET /backend/galleries/new
  def new
    @gallery = Backend::Gallery.new
  end

  # GET /backend/galleries/1/edit
  def edit
  end

  # POST /backend/galleries
  def create
    @gallery = Backend::Gallery.new(gallery_params)

    if @gallery.save
      unless gallery_params[:gallery_items].blank?
        gallery_params[:gallery_items].each do |i|
          image = GalleryItem.new
          image.file = i
          image.save
          @gallery.gallery_items << image
        end
        @gallery.save
      end
      redirect_to @gallery, notice: 'Галерея была успешно создана.'
    else
      render :new
    end
  end

  # PATCH/PUT /backend/galleries/1
  def update
    if @gallery.update(gallery_params)
      redirect_to @gallery, notice: 'Галерея успешно обновлена.'
    else
      render :edit
    end
  end

  # DELETE /backend/galleries/1
  def destroy
    @gallery.destroy
    redirect_to backend_galleries_url, notice: 'Галерея успешно удалена.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_backend_gallery
      @gallery = Backend::Gallery.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def gallery_params
      params.require(:backend_gallery).permit(:name, :title, :published,  gallery_items_attributes: [:id, :image, :_destroy])
    end
end
