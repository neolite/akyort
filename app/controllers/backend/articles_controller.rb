class Backend::ArticlesController < BackendController
  before_action :set_article, only: [:show, :edit, :update, :destroy]

  # GET /articles
  def index
    @articles = Backend::Article.all
  end

  # GET /articles/1
  def show
  end

  # GET /articles/new
  def new
    @article = Backend::Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles
  def create
    @article = Backend::Article.new(article_params)

    if @article.save
      redirect_to @article, notice: 'Запись была успешно создана'
    else
      render :new
    end
  end

  # PATCH/PUT /articles/1
  def update
    if @article.update(article_params)
      redirect_to @article, notice: 'Запись была успещно обновлена.'
    else
      render :edit
    end
  end

  # DELETE /articles/1
  def destroy
    @article.destroy
    redirect_to backend_article_url, notice: 'Запись была успешно удалена.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Backend::Article.friendly.find(params[:id])
      @article.category = :article
      @article.save
    end

    # Only allow a trusted parameter "white list" through.
    def article_params
      params.require(:backend_article).permit(:title, :seo_desc, :seo_kwords, :image, :content, :category, :status)
    end
end
