class Frontend::HousesController < ApplicationController
  layout 'frontend'
  before_action :set_house, only: [:show]

  def index
    @houses = House.all
  end

  def show

  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_house
    @house = House.friendly.find(params[:id])
  end

end
