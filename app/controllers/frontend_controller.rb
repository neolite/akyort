class FrontendController < ApplicationController
  def index
    @houses = House.all
    render :layout => false
  end
end
