require 'test_helper'

class Backend::GalleriesControllerTest < ActionController::TestCase
  setup do
    @backend_gallery = backend_galleries(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:backend_galleries)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create backend_gallery" do
    assert_difference('Backend::Gallery.count') do
      post :create, backend_gallery: { name: @backend_gallery.name, published: @backend_gallery.published }
    end

    assert_redirected_to backend_gallery_path(assigns(:backend_gallery))
  end

  test "should show backend_gallery" do
    get :show, id: @backend_gallery
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @backend_gallery
    assert_response :success
  end

  test "should update backend_gallery" do
    patch :update, id: @backend_gallery, backend_gallery: { name: @backend_gallery.name, published: @backend_gallery.published }
    assert_redirected_to backend_gallery_path(assigns(:backend_gallery))
  end

  test "should destroy backend_gallery" do
    assert_difference('Backend::Gallery.count', -1) do
      delete :destroy, id: @backend_gallery
    end

    assert_redirected_to backend_galleries_path
  end
end
