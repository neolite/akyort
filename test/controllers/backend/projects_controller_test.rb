require 'test_helper'

class Backend::ProjectsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get show" do
    get :show
    assert_response :success
  end

  test "should get set_project" do
    get :set_project
    assert_response :success
  end

end
