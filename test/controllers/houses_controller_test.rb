require 'test_helper'

class HousesControllerTest < ActionController::TestCase
  setup do
    @house = houses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:houses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create house" do
    assert_difference('House.count') do
      post :create, house: { city_name: @house.city_name, content: @house.content, desc: @house.desc, etaj: @house.etaj, home_size: @house.home_size, keywords: @house.keywords, land_area: @house.land_area, location_id: @house.location_id, material: @house.material, price: @house.price, price_for_meter: @house.price_for_meter, published: @house.published, slug: @house.slug, sold: @house.sold, title: @house.title, type: @house.type }
    end

    assert_redirected_to house_path(assigns(:house))
  end

  test "should show house" do
    get :show, id: @house
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @house
    assert_response :success
  end

  test "should update house" do
    patch :update, id: @house, house: { city_name: @house.city_name, content: @house.content, desc: @house.desc, etaj: @house.etaj, home_size: @house.home_size, keywords: @house.keywords, land_area: @house.land_area, location_id: @house.location_id, material: @house.material, price: @house.price, price_for_meter: @house.price_for_meter, published: @house.published, slug: @house.slug, sold: @house.sold, title: @house.title, type: @house.type }
    assert_redirected_to house_path(assigns(:house))
  end

  test "should destroy house" do
    assert_difference('House.count', -1) do
      delete :destroy, id: @house
    end

    assert_redirected_to houses_path
  end
end
