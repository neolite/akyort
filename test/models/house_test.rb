# == Schema Information
#
# Table name: houses
#
#  id              :integer          not null, primary key
#  title           :string(255)
#  slug            :string(255)
#  keywords        :string(255)
#  desc            :string(255)
#  price           :float(24)
#  price_for_meter :float(24)
#  house_type      :string(255)
#  material        :string(255)
#  etaj            :integer
#  content         :text(65535)
#  home_size       :float(24)
#  land_area       :float(24)
#  city_name       :string(255)
#  location_id     :integer
#  published       :boolean
#  sold            :boolean
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require 'test_helper'

class HouseTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
